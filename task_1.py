string = """The built-in string class provides the ability to do complex variable substitutions and value formatting via the format() method described in PEP 3101. The Formatter class in the string module allows you to create and customize your own string formatting behaviors using the same implementation as the built-in format() method.

class string.Formatter
The Formatter class has the following public methods:

format(format_string, /, *args, **kwargs)
The primary API method. It takes a format string and an arbitrary set of positional and keyword arguments. It is just a wrapper that calls vformat().

Changed in version 3.7: A format string argument is now positional-only."""

frequency_for_symbols = {}
number_of_symbols = len(string)

for symbol in set(string):
    frequency_for_symbols[symbol] = string.count(symbol) / number_of_symbols

results_list = list(frequency_for_symbols.items())

for key, result in results_list:
    print(f"{key}:   {result}")

table = str.maketrans(".,:;-_()[]", "          ")
