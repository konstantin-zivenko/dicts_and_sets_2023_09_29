d1 = {
    "one": 1,
    "two": 2,
    "three": 3,
}

d2 = dict(
    (
        (1, "one"),
        (2, "two"),
        (3, "three"),
    )
)

d2 = dict(
    [
        'ab',
        'cd',
        'ef',
    ]
)
